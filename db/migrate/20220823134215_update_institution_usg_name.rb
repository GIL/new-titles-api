class UpdateInstitutionUsgName < ActiveRecord::Migration[6.0]
  def up
    usg = Institution.find_by_shortcode('usg')
    usg.name = "University System of Georgia"
    usg.save!
  end

  def down
    usg = Institution.find_by_shortcode('usg')
    usg.name = "Board of Regents of the University System of Georgia"
    usg.save!
  end
end
