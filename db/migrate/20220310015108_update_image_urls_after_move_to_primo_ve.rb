class UpdateImageUrlsAfterMoveToPrimoVe < ActiveRecord::Migration[5.2]
  def up
    Institution.all.each do |inst|
      inst.image.sub!('primo-explore/custom/', 'discovery/custom/' + inst.institution_code + '-')
      inst.image.sub!('galileo-usg-', 'galileo-')
      inst.image.sub!('-primo.hosted.', '.primo.')
      inst.save!
    end

    inst = Institution.find_by(institution_code: '01GALI_ALBANY')
    inst.image.sub!('-asu.', '-asurams.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_AUG')
    inst.image.sub!('-augusta.', '-aug.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_COLUMBUS')
    inst.image.sub!('-csu.', '-columbusstate.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_DALTON')
    inst.image.sub!('-dalton.', '-daltonstate.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_EAGEORG')
    inst.image.sub!('-egsc.', '-ega.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GADEPT')
    inst.image.sub!('-archives.', '-georgiaarchives.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_FLOYD')
    inst.image.sub!('-ghc.', '-highlands.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GASOUTH')
    inst.image.sub!('-gasou.', '-georgiasouthern.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GSSU')
    inst.image.sub!('-gswu.', '-gsw.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GORDON')
    inst.image.sub!('-gordon.', '-gordonstate.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GIT')
    inst.image = 'https://galileo-gatech.primo.exlibrisgroup.com/discovery/custom/thumbnails/thumbnail_01GALI_GIT-GT.png'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_KENNESAW')
    inst.image.sub!('-ksu.', '-kennesaw.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_SSU')
    inst.image.sub!('-ssu.', '-savannahstate.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_UWG')
    inst.image.sub!('-uwg.', '-westga.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_VALDOSTA')
    inst.image.sub!('-vsu.', '-valdosta.')
    inst.save!
  end

  def down
    Institution.all.each do |inst|
      inst.image.sub!('.primo.', '-primo.hosted.')
      inst.image.sub!('galileo-',  'galileo-usg-')
      inst.image.sub!('discovery/custom/' + inst.institution_code + '-', 'primo-explore/custom/')
      inst.save!
    end

    inst = Institution.find_by(institution_code: '01GALI_ALBANY')
    inst.image.sub!('-asurams.', '-asu.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_AUG')
    inst.image.sub!('-aug.', '-augusta.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_COLUMBUS')
    inst.image.sub!('-columbusstate.', '-csu.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_DALTON')
    inst.image.sub!('-daltonstate.', '-dalton.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_EAGEORG')
    inst.image.sub!('-ega.', '-egsc.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GADEPT')
    inst.image.sub!('-georgiaarchives.', '-archives.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_FLOYD')
    inst.image.sub!('-highlands.', '-ghc.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GASOUTH')
    inst.image.sub!('-georgiasouthern.', '-gasou.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GSSU')
    inst.image.sub!('-gsw.', '-gswu.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GORDON')
    inst.image.sub!('-gordonstate.', '-gordon.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GIT')
    inst.image = 'http://www.library.gatech.edu/images/gt-logo-solid-blue.png'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_KENNESAW')
    inst.image.sub!('-kennesaw.', '-ksu.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_SSU')
    inst.image.sub!('-savannahstate.', '-ssu.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_UWG')
    inst.image.sub!('-westga.', '-uwg.')
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_VALDOSTA')
    inst.image.sub!('-valdosta.', '-vsu.')
    inst.save!
  end
end
