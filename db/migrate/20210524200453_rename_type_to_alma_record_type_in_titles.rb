class RenameTypeToAlmaRecordTypeInTitles < ActiveRecord::Migration[5.2]
  def change
  	rename_column :titles, :type, :alma_record_type
  end
end
