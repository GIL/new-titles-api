class QuickFixForUrlsAfterMoveToPrimoVe < ActiveRecord::Migration[5.2]
  def up
    Institution.all.each do |inst|
      inst.url.gsub!('&query=addsrcrid', '&query=any')
      inst.save!
    end
  end

  def down
    Institution.all.each do |inst|
      inst.url.gsub!('&query=any', '&query=addsrcrid')
      inst.save!
    end
  end
end
