class AddTypeToTitles < ActiveRecord::Migration[5.2]
  def change
    add_column :titles, :type, :string
  end
end
