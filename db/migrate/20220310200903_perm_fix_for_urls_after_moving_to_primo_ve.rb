class PermFixForUrlsAfterMovingToPrimoVe < ActiveRecord::Migration[5.2]
  def up
    inst = Institution.find_by(institution_code: '01GALI_GADEPT')
    inst.url = 'https://galileo-georgiaarchives.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GADEPT:ARCHIVES_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_BALDWIN')
    inst.url = 'https://galileo-abac.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_BALDWIN:ABAC_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_ALBANY')
    inst.url = 'https://galileo-asurams.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_ALBANY:ASU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_ATLAMETRO')
    inst.url = 'https://galileo-atlm.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_ATLAMETRO:ATLM_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_AUG')
    inst.url = 'https://galileo-aug.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_AUG:AUGUSTA_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_CLAYTON')
    inst.url = 'https://galileo-clayton.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_CLAYTON:CLAYTON_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_COASTLGA')
    inst.url = 'https://galileo-ccga.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_COASTLGA:CCGA_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_COLUMBUS')
    inst.url = 'https://galileo-columbusstate.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_COLUMBUS:CSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_DALTON')
    inst.url = 'https://galileo-daltonstate.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_DALTON:DALTON_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_EAGEORG')
    inst.url = 'https://galileo-ega.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_EGSC:EGSC_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_FTVALLEY')
    inst.url = 'https://galileo-fvsu.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_FTVALLEY:FVSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GIT')
    inst.url = 'https://galileo-gatech.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GIT:GT&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GASOUTH')
    inst.url = 'https://galileo-georgiasouthern.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GASOUTH:GASOU&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GCSU')
    inst.url = 'https://galileo-gcsu.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GCSU:GCSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GWINNETT')
    inst.url = 'https://galileo-ggc.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GWINNETT:GGC_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_FLOYD')
    inst.url = 'https://galileo-highlands.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_FLOYD:GHC_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GORDON')
    inst.url = 'https://galileo-gordonstate.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GORDON:GORDON_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GSU')
    inst.url = 'https://galileo-gsu.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GSU:GSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_GSSU')
    inst.url = 'https://galileo-gsw.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_GSSU:GSWU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_KENNESAW')
    inst.url = 'https://galileo-kennesaw.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_KENNESAW:KSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_MGA')
    inst.url = 'https://galileo-asurams.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_MGA:MGA_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_SSU')
    inst.url = 'https://galileo-mga.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_SSU:SSU_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_SGC')
    inst.url = 'https://galileo-asurams.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_SGC:SGSC_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_UGA')
    inst.url = 'https://galileo-uga.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_UGA:UGA&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_UNG')
    inst.url = 'https://galileo-ung.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_UNG:UNG_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_UWG')
    inst.url = 'https://galileo-westga.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_UWG:UWG_V1&search_scope=MyInstitution&docid=alma'
    inst.save!
    inst = Institution.find_by(institution_code: '01GALI_VALDOSTA')
    inst.url = 'https://galileo-valdosta.primo.exlibrisgroup.com/discovery/fulldisplay?vid=01GALI_VALDOSTA:VSU&search_scope=MyInstitution&docid=alma'
    inst.save!
  end

end
