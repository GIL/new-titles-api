source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '3.2.2'

gem 'bootsnap', '>= 1.4.2', require: false
gem 'httparty'
gem 'jbuilder', '~> 2.7'
gem 'jquery-datatables-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 7.0.0'
gem 'kaminari'
gem 'nokogiri', '~> 1.16.5'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 6.4.3'
gem 'rack-cors', '~> 2.0.2'
gem 'rails', '~> 7.0.8.7'
gem 'slack-notifier'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'sqlite3', '~> 1.4'
end

group :test do
  gem 'database_cleaner'
  gem 'fabrication'
  gem 'faker'
  gem 'rspec-rails'
  gem 'simplecov'
  gem 'webmock', '~> 2.1'
end
