# Alma Analytics Reports

The files contained in this directory are XML backups of the reports that have been set up in Alma Analytics.
The New Titles application requires these reports, running them when updating new titles data.
In the unlikely event that these reports need to be rebuilt in Alma Analytics, you can simply cope the XML into the
"Analysis XML" textbox under the Advanced tab in the analysis editor.  

These reports are located in the following area in the Analytics Catalog for all USG institutions:

`/Shared Folders/[Institution]/New Titles Electronic`  
`/Shared Folders/[Institution]/New Titles Physical`

If these reports are updated in Alma, _please update their XML backups in this directory_.
