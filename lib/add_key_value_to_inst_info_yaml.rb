# Used this to easily add a new field to the inst_info.yml file.  Can also be used to easily generate code for
# creating data changes in the DB that apply to all institutions.
require 'yaml'

@data = YAML.load_file "config/inst_info.yml"

@data['inst_data'].each do |inst, data|
  #use this to create migrations:
  #puts "inst = Institution.find_by(institution_code: '" + data['alma_inst_code'] + "')"
  #puts "inst.url = 'https://" + data['primo_domain'] + "/discovery/fulldisplay?vid=" + data['alma_inst_code'] + ":" + data['primo_view'] + "&search_scope=MyInstitution&docid=alma'"
  #puts "inst.save!"
  # use this to add a field to YAML:
  @data['inst_data'][inst]['primo_permalink_base'] = "https://" + data['primo_domain'] + "/discovery/fulldisplay?vid=" + data['alma_inst_code'] + ":" + data['primo_view'] + "&search_scope=MyInstitution&docid=alma"
end
# Once done manipulating, dump it back with YAML.dump
# to convert it back to YAML.
output = YAML.dump @data
File.write("config/new.yml", output)