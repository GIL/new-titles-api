# Run like this:
# rails export:export_to_seeds > db/seeds_new.rb
# Once you confirm the export looks good, replace the existing seeds file
namespace :export do
  desc "Export Institutions"
  task :export_to_seeds => :environment do
    Institution.all.each do |inst|
      excluded_keys = %w[created_at updated_at id]
      serialized = inst.serializable_hash.delete_if { |key,value| excluded_keys.include?(key) }
      puts "Institution.create!(#{serialized})"
    end
  end
end