class TableController < ApplicationController
  def index

    @institutions = Institution.all
    @institution = Institution.find_by_shortcode(params[:shortcode])

    sql = "SELECT DISTINCT(material_type) FROM titles" + (@institution && @institution.shortcode != 'usg' ? " where institution_id=#{@institution.id}" : "") +
     " ORDER BY material_type"
    records = ActiveRecord::Base.connection.exec_query(sql)
    @media_types = records.rows
    @media_types.unshift ['--','']

    @libraries = []
    if @institution and @institution.shortcode != 'usg'
      sql = "SELECT DISTINCT(library) FROM titles where institution_id=#{@institution.id} and library != '' ORDER BY library"
      records = ActiveRecord::Base.connection.exec_query(sql)
      @libraries = records.rows
      @libraries.unshift ['--','']
    end

    respond_to do |format|
      format.html
      format.json { render json: TitlesDatatable.new(view_context, @institution) }
    end
  end
end
