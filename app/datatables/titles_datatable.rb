class TitlesDatatable
  delegate :params, to: :@view

  def initialize(view, institution = nil)
    @view = view
    @institution = institution
  end

  def as_json(*)
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Title.count,
      iTotalDisplayRecords: titles.total_count,
      aaData: data
    }
  end

  private

  def data
    titles.map do |title|
      [
        title.receiving_date,
        #title.isbn ? '<div class="thumbnail" data-isbn="' + title.isbn + '"><img src=""/></div>' : nil,
        title.isbn ? '<div class="thumbnail"><img src="https://secure.syndetics.com/index.aspx?isbn=' + get_isbn_for_thumbnail(title.isbn) + '/mc.gif&client=galileogil&type=unbound"/>' : nil,
        title.title ? "<a href='#{title.institution.url}#{title.mms_id}'>#{title.title.titleize}</a>" : nil,
        title.author ? title.author.titleize: nil,
        title.material_type,
        title.publisher ? title.publisher.titleize : nil,
        title.call_number,
        title.call_number_sort,
        title.library,
        if institution_specified?
          title.location
        else
          title.inst_name
        end
      ]
    end
  end

  def titles
    @titles ||= fetch_titles
  end

  def fetch_titles
    titles = Title.order("#{sort_column} #{sort_direction}")
    titles = titles.page(page).per(per_page)
    titles = titles.where(institution: @institution) if institution_specified?
    if params[:sSearch].present?
      titles = titles.where(
        'LOWER(title) like :search or LOWER(author) like :search or LOWER(publisher) like :search or LOWER(material_type) like :search or LOWER(call_number) like :search or LOWER(mms_id) like :search or LOWER(location) like :search',
        search: "%#{params[:sSearch]}%".downcase
      )
    end
    if params[:media_type].present?
      #media_types = media_types_map[params[:media_type].downcase.to_sym]
      #titles = titles.where(material_type: media_types) if media_types&.any?
      titles = titles.where(material_type: params[:media_type])
    end

    if params[:receiving_date].present? and params[:receiving_date] != ''
      titles = titles.where('receiving_date > ?', params[:receiving_date].to_i.days.ago)
    end

    if params[:library].present? and params[:library] != ''
      titles = titles.where(library: params[:library])
    end

    if params[:lc_class].present? and params[:lc_class] != ''
      titles = titles.where('classification_code like :code', code: "#{params[:lc_class]}%")
    end

    if params[:record_type].present? and params[:record_type] != ''
      titles = titles.where(alma_record_type: params[:record_type])
    end

    titles.includes(:institution)
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i.positive? ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[receiving_date isbn title author material_type publisher call_number call_number_sort_hidden library]
    columns << if @institution
                 'location'
               else
                 'institutions.name'
               end
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end

  def institution_specified?
    @institution && !@institution.usg?
  end

  def get_isbn_for_thumbnail isbn
    isbn.split('; ').last
  end

  def media_types_map
    {
      dvd: ['Blu-Ray', 'Blu-Ray and DVD', 'DVD', 'DVD-ROM'],
      music: ['Sound Recording', 'Audio cassette', 'LP', 'Phonograph Record'],
      book: ['Book'],
      map: ['Map', 'Atlas'],
      device: ['Tablet', 'Calculator', 'Camcorder', 'Tablet', 'iPad', 'Laptop'],
      unknown: ['Unknown'],
      none: ['None'],
      thesis: ['Thesis', 'Master Thesis', 'Dissertation', 'PhD Thesis'],
      issue: ['Issue'],
      journal: ['Journal']
    }
  end
end