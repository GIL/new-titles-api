# GIL New Titles API

## Use

USG Member Institutions can request an API key from [here](https://gil.usg.edu/contact/apirequest).

A basic front-end powered by jQuery Datatables can be found at <https://newtitles.gil.usg.edu>

### Sending Requests

_See the [titles spec file](spec/request/titles_spec.rb) for more guidance_

Requests are sent to `https://newtitles.gil.usg.edu/api/v1/list`

Your API Key should be sent in the HTTP Headers as:

```'X-User-Token': YOUR_API_KEY```

The API Key will limit the returned records to only those from your institution.  Note that API keys are generated when the database is initialized.
Note that this key is unrelated to the Alma API key.  These keys are randomized on the production instance, but in development are set to institution_code_key (example: 01GALI_UGA_key for UGA)

Optionally, you can send a parameter to specify a media_type:

`https://newtitles.gil.usg.edu/api/v1/list?media_type=DVD`

Or, you can limit by location value (be sure to URL encode values):

`https://newtitles.gil.usg.edu/api/v1/list?location=Main+Library+-+Second+Floor+%28Rotunda%29`

#### Media Types

See the [media type translation hash](app/controllers/titles_controller.rb#L36)
for all available parameter values and the corresponding Alma values.

### Response Object

The response will be a JSON array of title objects, e.g.:

```
[
    {
        "title":"Title of New Title", 
        "author":"Author", 
        "publisher":"Publisher", 
        "call_number":"LC Call number", 
        "library":"Library at your Institutions", 
        "location":"Location at your Institution", 
        "material_type":"Alma media type", 
        "receiving_date":"2018-09-05", 
        "mms_id":"5096308454493377", 
        "subjects":"Comma, separated, subjects", 
        "isbn":"ISBN, if present", 
        "publication_date":"Publication Date, if provided", 
        "portfolio_name":"For elecronic records", 
        "portfolio_activation_date":"For electronic records", 
        "portfolio_creation_date":"For electronic records", 
        "classification_code":"LC Classification code, if available", 
        "availability":"Alma title availability", 
        "call_number_sort":"Sortable version of call number", 
        "inst_name":"Your Institution"
    }
]
```

## The Alma Analytics reports

The data in the New Titles API is harvested from Alma via two Alma Analytics reports: _New Titles Electronic_, and _New Titles Physical_.
In production, these reports are run when a Cron job calls a shell script which calls the _get_new_titles_ rake task, 
which runs the reports via the Alma Analytics API and stores the result in the database.  XML representations of these reports are available 
in the /alma_reports directory.  These can be used in the unlikely event that it becomes necessary to rebuild the reports in the Alma Analytics web interface.
See the README in that directory for more information.  Note: be sure the rails server isn't running when running this script, as you may get the error `FATAL: Listen error: unable to monitor directories for changes.`

These reports are located in the following area in the Analytics Catalog for all USG institutions:

`/Shared Folders/[Institution]/New Titles Electronic`  
`/Shared Folders/[Institution]/New Titles Physical`

Note that if the institutions make modifications to the reports, it will break the ingest, making it pull data into the wrong fields.  There is an option to make reports read-only in Alma, but note that if you do so you cannot undo this (as of this writing).  This is a bug in Alma Analytics.  If you make a report read-online, and you need to modify the report, you will need to first make a copy, then you'll be able to uncheck "Read Only", then delete the original report and rename the copy back to the original name.  This is important, as the reports are called from the rake task based on their path and name.


## The Rake task used to pull new titles data from Alma:

If you need to call the rake task manually on production, be sure you're logged in as gitlab-runner, and run this (example is for Georgia Highlands):

`RAILS_ENV=production bundle exec rake "get_new_titles[ghc, electronic]"`


## Setting up a local development instance

- Clone the code
- Create or obtain the `config/secrets.yml` file from either the production instance or another developer (contains Alma API keys etc)
- Optional: obtain he `config/`
- Create a `config/database.yml` that looks like this: https://gist.github.com/danopia/940155
- Run bundle install 
- Run RAILS_ENV=development bundle exec rake db:setup
- Start the development server in Rubymine, or Run: RAILS_ENV=development bundle exec rails server to see the site on port 3000

At this point the app should be working but there will be no data populated.  Look at the new_titles_development.sh script included with the project, 
it's basically the same script that runs on the production server to pull in data, but with the rails env hard coded to development.
It is recommended to switch between lines 2 and 5, commenting out the full list of institutions, and uncommenting the short list.
It takes a while to run on all of them because it's loading up data from Alma.  The shorter one just loads the bigger institutions.
Periodically re-run this to freshen the data.
